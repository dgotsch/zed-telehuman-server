/*
Copyright (c) 2016 Daniel Gotsch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// The code below is a heavily modified version of STEREOLABS example found at https://github.com/stereolabs/zed-multi-input
*/

// local definitions
#include "telehuman-network/udp_network.h"

//standard includes
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <iterator>
#include <ctime>
#include <chrono>
#include <thread>
#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <chrono>
#include <cmath>

//opencv includes
#include <opencv2/opencv.hpp>

//OS includes
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <net/if.h>

//ZED Includes
#include <sl/Camera.hpp>


#define RECV_BUFSIZE 64
#define RECV_PORT 15001

using namespace std;

class UDPSenderReceiver {

private:
	int fd_out_; 
	struct sockaddr_in to_addr_;
	vector<unsigned char> data_buffer_;
	thread* listener_;
	th::Packet packet_;
	
	
	void receiveVector3( unsigned char* buf) {
		//modify data type

		//send data to odroids
		//cout << "unpacking" << endl;
		float x = *(float*)&buf[1];
		float y = *(float*)&buf[5];
		float z = *(float*)&buf[9];
		//cout <<"unpacked" << endl;
		cout << "x " << x << " y " << y << " z " << z << endl;;
	}


	int listenThread()
	{
		struct sockaddr_in myaddr;
		struct sockaddr_in from_addr;
		socklen_t addrlen = sizeof(from_addr);
		int fd;

		//create a udp socket
		if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		{
			cerr << "Error: Cannot create listening socket." << endl;
			return -1;
		}

		//bind the socket to an IP address and a port
		memset((char*)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		myaddr.sin_port = htons(RECV_PORT);

		if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0)
		{
			cerr << "Error: Bind to listening socket failed." << endl;
			return -1;
		}

	
		unsigned char buf[RECV_BUFSIZE];

		while (true)
		{

			int recvlen = recvfrom(fd, buf, RECV_BUFSIZE, 0, (struct sockaddr *)(&from_addr), &addrlen);
		
		
			unsigned char type = *(unsigned char*)&buf[0];
			//cout << "got " << type << endl;
		
			switch (type)
			{
			case 'c':
				//cout << "receive vector3" << endl;
				receiveVector3(buf);
				break;
			default:
				break;
			}
		}
	
	}
	
public:

	int connectInSocket() {
	
		listener_ = new thread([this] { this->listenThread(); });
	}
	
	
	UDPSenderReceiver( size_t data_size) {
		data_buffer_.reserve( data_size );
	}

	~UDPSenderReceiver() {
		delete listener_;
	}

	int connectOutSocket( ) {


		struct sockaddr_in myaddr;
		const char *listener_address = "255.255.255.255";	/* broadcast address */
		const char *sender_address = "192.168.0.200"; /* the address we're sending from - useful if we have multiple interfaces */

		/* create a socket */
		fd_out_ = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd_out_ < 0) {
		    cerr << "ERROR: opening socket" << endl;
			return -1;
		}

		/* bind the socket to a particular interface - NOT USED */ 
		/*struct ifreq ifr;

		memset(&ifr, 0, sizeof(ifr));
		snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "enp0s31f6");
		if (setsockopt(fd_out_, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
			cerr << "ERROR: connecting socket at network interface 'enp3s0'" << endl;
			return -1;
		}	//*/

		/* turn on broadcasting */
		int broadcastEnable=1;
		int ret=setsockopt(fd_out_, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));


		/* bind it to our local address and pick any port number */
		memset((char *)&myaddr, 0, sizeof(myaddr));
		myaddr.sin_family = AF_INET;
		if (inet_aton(sender_address, &myaddr.sin_addr)==0) {
			cerr << "ERROR: inet_aton() failed" << endl;
			return -1;
		}
	
		myaddr.sin_port = SERVICE_PORT;

		if (bind(fd_out_, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
			perror("ERROR: bind failed");
			return -1;
		}    

		/* now define to_addr_, the address to whom we want to send messages */
		/* For convenience, the host address is expressed as a numeric IP address */
		/* that we will convert to a binary format via inet_aton */

		memset((char *) &to_addr_, 0, sizeof(to_addr_));
		to_addr_.sin_family = AF_INET;
		to_addr_.sin_port = htons(SERVICE_PORT);
		if (inet_aton(listener_address, &to_addr_.sin_addr)==0) {
			cerr << "ERROR: inet_aton() failed" << endl;
			return -1;
		}

		cout << "UDP socket created succesfully." << endl;
	}



	void broadcastVectorData( vector<uchar>& data, th::PacketType type, int zed_id, long frame_id ) {
	
		packet_.setHeader( zed_id, type, frame_id );
		packet_.sendVector( fd_out_, data, to_addr_);
		
	}
	
	
	void sendFrame( cv::Mat& colour, cv::Mat& depth, vector<int> comp_params, int zed_id, long frame_id ) {
	
		cv::imencode(".jpg", colour, data_buffer_, comp_params);
		broadcastVectorData( data_buffer_, th::PacketType::ZedFrameColour, zed_id, frame_id );
		
		cv::imencode(".jpg", depth, data_buffer_, comp_params);
		broadcastVectorData( data_buffer_, th::PacketType::ZedFrameDepth, zed_id, frame_id );
	}
	
	void sendManualOffsets( unsigned char odroid_id, float x_off, float y_off, float r_off ) {

		struct th::ManualOffsets data;

		data.x_off = x_off;
		data.y_off = y_off;
		data.r_off = r_off;
		
		packet_.setHeader( odroid_id, th::PacketType::ManualOffsets, 0 );
		packet_.sendStruct<struct th::ManualOffsets>( fd_out_, data, to_addr_ );
				
	}

	void sendZedPositions(cv::Mat_<float> zed_pos, cv::Mat_<float> look_at, cv::Mat_<float> up_vect,  int zed_id ) {
	
		struct th::ZedConfig data;

		// this is equivalent to 		memcpy( &data.zed_pos, zed_pos.data, sizeof(data.zed_pos));		
		data.zed_pos = * ((std::array<float, 3> *) zed_pos.data);
		data.look_at = * ((std::array<float, 3> *) look_at.data);
		data.up_vect = * ((std::array<float, 3> *) up_vect.data);
		
		//Resolution image_size = zed[x]->getResolution();
		//data.
		//CamParameters zed_params = zed[x]->getParameters()->LeftCam;

		packet_.setHeader( zed_id, th::PacketType::ZedConfig, 0 );
		packet_.sendStruct<struct th::ZedConfig>( fd_out_, data, to_addr_ );

	}


};

