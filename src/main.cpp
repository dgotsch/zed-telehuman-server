/*
Copyright (c) 2016 Daniel Gotsch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// The code below is a heavily modified version of STEREOLABS example found at https://github.com/stereolabs/zed-multi-input
*/


///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2015, STEREOLABS.
//
// All rights reserved.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////


/******************************************************************************************************************
 ** This sample demonstrates how to use two ZEDs with the ZED SDK, each grab are in a separated thread			**
 ** This sample has been tested with 3 ZEDs in HD720@30fps resolution											 **
 ** This only works for Linux																					 **
 *******************************************************************************************************************/
// local definitions
#include "network.cpp"
#include "kernel.cuh"
#include "mesh_renderer.cpp"

//standard includes
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <iterator>
#include <ctime>
#include <chrono>
#include <thread>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <chrono>
#include <cmath>
#include <algorithm>

//OS includes
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <net/if.h>

//opencv includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/calib3d.hpp>
//#include <opencv2/cudev.hpp>


//opengl includes
#include <glm/vec3.hpp> // glm::vec3
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

//cuda
#include <cuda_runtime.h>
#include <cuda_profiler_api.h>

//#include <helper_cuda.h>

//ZED Includes
#include <sl/Camera.hpp>



//using namespace sl;
using namespace std;
using namespace chrono;


// this is useful for timing
using to_ms_dur = duration<double, milliseconds::period>;
#define TO_MS(x) to_ms_dur(x).count()

// other macros

// Macro to convert a ZED sl::mat stored on the GPU to an OpenCV GpuMat
#define SLGPUMAT2CVGPUMAT( mat, type ) \
cv::cuda::GpuMat(mat.getHeight(), mat.getWidth(), type, mat.getPtr<sl::uchar1>(sl::MEM_GPU), mat.getStepBytes(sl::MEM_GPU));

#define SLCPUMAT2CVMAT( mat, type ) \
cv::Mat(mat.getHeight(), mat.getWidth(), type, mat.getPtr<sl::uchar1>(sl::MEM_CPU));

// input constants
const int NUM_CAMERAS = 3;
const int NUM_GPUS = 2;
const int FPS = 15;
const sl::RESOLUTION ZED_RES = sl::RESOLUTION_HD2K;

// output constants
const float OUT_FPS = 10.0;
const cv::Size depth_size = cv::Size(960, 540);//cv::Size(960, 540);//  cv::Size(1280, 720);
const cv::Size colour_size = cv::Size(960, 540);// cv::Size(2208, 1242);// cv::Size(1280, 720);
const cv::Size display_size = cv::Size(450, 800);// cv::Size(540, 960);//

// calibration constants
cv::Size board_size = cv::Size(7,9);//cv::Size(4,11);
float board_spacing = 0.05; //in meters


// global variables for inter thread communication
sl::Camera zed[NUM_CAMERAS];
cv::Mat colour_result[NUM_CAMERAS];
cv::Mat depth_result[NUM_CAMERAS];
cv::Mat colour_display[NUM_CAMERAS];
cv::Mat depth_display[NUM_CAMERAS];
cv::Mat zed_display;

long long zed_timestamp[NUM_CAMERAS];
glm::mat4 zed_to_world_mat[NUM_CAMERAS];



cv::Mat calibration_image = cv::imread("../src/calibration.png");
cv::Mat calibration_depth = cv::imread("../src/calibration_depth.png");






bool stop_signal, grab_background, be_sending_data = 0,  be_updating_data = 1, be_removing_background = 1;
vector<int>  save_image(NUM_CAMERAS, -1);
int bkgd_thresh = 35, gain, exposure;
float depth_min = 1.0, depth_max = 20.0;

// network variables


int mapZed2GPU(int x){
	return x % NUM_GPUS;
}


void printZedSettings(int x) {
	sl::CAMERA_SETTINGS setting_value [] = {sl::CAMERA_SETTINGS_BRIGHTNESS, sl::CAMERA_SETTINGS_CONTRAST, sl::CAMERA_SETTINGS_HUE, sl::CAMERA_SETTINGS_SATURATION, sl::CAMERA_SETTINGS_GAIN, sl::CAMERA_SETTINGS_EXPOSURE, sl::CAMERA_SETTINGS_WHITEBALANCE };
	string setting_name [] = {"Bri", "Con", "Hue", "Sat", "Gain", "Exp", "WB"};

	cout << "ZED N°" << x;
	for(int i = 0; i < 7; i++) {
		cout << " " << setting_name[i] << ": " << zed[x].getCameraSettings(setting_value[i]);
	}
	cout << endl;
}


void autoAdjustZedSettings() {

	// Set default settings
	gain = 0;
	exposure = 100;

	// Get auto camera settings  // TODO: this doesn't seem to work in v2.0.1
	for (int i = 0; i < NUM_CAMERAS; i++) {
		zed[i].setCameraSettings(sl::CAMERA_SETTINGS_EXPOSURE, -1, true); // Set Auto exposure and gain
		zed[i].setCameraSettings(sl::CAMERA_SETTINGS_GAIN, -1, true);

		// read the settings
		//gain = max(gain, zed[i].getCameraSettings(ZED_GAIN));
		//if(zed[i].getCameraSettings(CAMERA_SETTINGS_EXPOSURE) >= 0 )
		//	exposure = min(exposure, zed[i].getCameraSettings(CAMERA_SETTINGS_EXPOSURE)  );

	}

	cout << " setting gain: " << gain << " exp: " << exposure << " depth_max: " << depth_max << endl;

	// Set the camera settings
	for (int i = 0; i < NUM_CAMERAS; i++) {

		zed[i].setCameraSettings(sl::CAMERA_SETTINGS_EXPOSURE, exposure); // Exposure must be first
		zed[i].setCameraSettings(sl::CAMERA_SETTINGS_GAIN, gain);
		//CAMERA_SETTINGS_WHITEBALANCE
		//CAMERA_SETTINGS_AUTO_WHITEBALANCE 0
		zed[i].setDepthMaxRangeValue(depth_max);
		//zed[i].setConfidenceThreshold(90);

		printZedSettings(i);
	}
}

void drawAxis( cv::Mat image, cv::InputArray rvec, cv::InputArray tvec, cv::InputArray cam_mat, cv::InputArray dist_coeffs) {

	const float axl = 0.2;
	vector<cv::Point3f> axis;
	axis.push_back(cv::Point3f(0.0,0.0,0.0)); // o
	axis.push_back(cv::Point3f(axl,0.0,0.0)); // x
	axis.push_back(cv::Point3f(0.0,axl,0.0)); // y
	axis.push_back(cv::Point3f(0.0,0.0,axl)); // z
	vector<cv::Point2f> axis_proj;

	cv::projectPoints(axis, rvec, tvec, cam_mat, dist_coeffs, axis_proj);

	cv::line(image, axis_proj[0], axis_proj[1], cv::Scalar(0,0,255), 3); // red
	cv::line(image, axis_proj[0], axis_proj[2], cv::Scalar(0,255,0), 3); // green
	cv::line(image, axis_proj[0], axis_proj[3], cv::Scalar(255,0,0), 3); // blue

}

vector<cv::Point3f> generateBoard() {

	vector<cv::Point3f> points;
	glm::vec3 off(board_spacing/2.0f*-(float)(board_size.height-1), 0.00, board_spacing/2.0f*(2.0f*(float)(board_size.width-1)) );
	//cout << "board off = " << to_string( off ) << endl;

	for( int i = 0; i < board_size.height; i++ )
		for( int j = 0; j < board_size.width; j++ )
			points.push_back(cv::Point3f(i *board_spacing + off.x, off.y, -(2*j + i % 2)*board_spacing + off.z));

	return points;
}

bool calibrateZedToWorld( int x, cv::Mat_<float>& zed_pos, cv::Mat_<float>& look_at, cv::Mat_<float>& up_vect, float& pos_angle )
{
	// grab a fresh image from the left camera and extract the red channel
	cv::Mat channels[4], view;
	sl::Mat temp;


	sl::Resolution image_size = zed[x].getResolution();
	sl::Mat image_zed(image_size, sl::MAT_TYPE_8U_C4); // Create a sl::Mat to handle Left image
	cv::Mat image = SLCPUMAT2CVMAT(image_zed, CV_8UC4);// Create an OpenCV Mat that shares sl::Mat buffer

	zed[x].retrieveImage(image_zed, sl::VIEW_LEFT );
	cv::split(image, channels);
	view = channels[0];

	// find the points on the image and show them
	vector<cv::Point2f> image_points;
	bool found = findCirclesGrid( view, board_size, image_points, cv::CALIB_CB_ASYMMETRIC_GRID );
	cv::drawChessboardCorners( image, board_size, image_points, found );
	//cv::imshow( "ZED No " + to_string(x) + " calibration image", image);
	if( !found ) {
		cerr << "ERROR: ZED N°" << x << " - no calibration pattern found!" << endl;
		cv::imshow( "ZED No " + to_string(x) + " calibration image", image);
		return false;
	}

	// populate the camera matrix and distortion parameter vector
	sl::CameraParameters zed_params = zed[x].getCameraInformation().calibration_parameters.left_cam;

	cv::Size s = image.size();
	double cam_mat_init[] = {zed_params.fx, 0.0, s.width/2.0,	 0.0, zed_params.fy, s.height/2.0,	 0.0, 0.0, 1.0 };
	// we are using image width and height since it should be rectified
	//double cam_mat_init[] = {zed_params.fx, 0.0, zed_params.cx,	 0.0, zed_params.fy, zed_params.cy,	 0.0, 0.0, 1.0 };
	cv::Mat cam_mat = cv::Mat( 3, 3, CV_64F, cam_mat_init);
	cv::Mat dist_coeffs = (cv::Mat_<double>(5, 1) << 0.0,0.0,0.0,0.0,0.0); // the captured ZED images are rectified

	cv::Ptr<cv::Formatter> formatMat=cv::Formatter::get(cv::Formatter::FMT_DEFAULT);
 	formatMat->set64fPrecision(4);
 	formatMat->set32fPrecision(4);


	cout << "cam_mat : " <<  formatMat->format( cam_mat ) << endl;
	//cout << "dist_coeffs : " << dist_coeffs << endl;

	// generate points that match the calibration board
	vector<cv::Point3f> object_points = generateBoard();

	// match the two sets of points
	cv::Mat rvec(3, 1, CV_64F), tvec(3,1, CV_64F), R;// set up output vectors
	//std::vector<double> r(3);

	/*vector<vector<cv::Point3f>> o; o.push_back(object_points);
	vector<vector<cv::Point2f>> i; i.push_back(image_points);
	vector<cv::Mat> r; r.push_back(rvec);
	vector<cv::Mat> t; t.push_back(tvec);
	cv::calibrateCamera(o, i, s, cam_mat, dist_coeffs, r, t, CV_CALIB_USE_INTRINSIC_GUESS);
	cout << "cam_mat : " <<  formatMat->format( cam_mat ) << endl;//*/

	cv::solvePnP(object_points, image_points, cam_mat, dist_coeffs, rvec, tvec);
//	cout<< "tvec1 " << formatMat->format( tvec.t() ) << endl;
//	cout<< "rvec1 " << formatMat->format( rvec.t() ) << endl;
//	cv::solvePnP(object_points, image_points, cam_mat, dist_coeffs, rvec, tvec, false, cv::SOLVEPNP_UPNP );
//	cout<< "tvec2 " << formatMat->format( tvec.t() ) << endl;
//	cout<< "rvec2 " << formatMat->format( rvec.t() ) << endl;
		//glm::vec3 r3= glm::make_vec3(r.data());

	// draw the axis
	drawAxis(image, rvec, tvec, cam_mat, dist_coeffs);
	//cv::imshow( "ZED No " + to_string(x) + " calibration image", image);

	// calculate camera position and orientation in world coordinates
	cv::Rodrigues(rvec, R);


	glm::mat3 rot = glm::make_mat3( R.ptr<double>());
	cout << "rot mat " << to_string( rot ) << endl<< " with determinant " << (glm::determinant(rot)) << endl;


	zed_pos = R.t()*(-tvec);
	look_at = R.t()*(-tvec + (cv::Mat_<double>(3, 1) << 0.0, 0.0, 1.0));
	up_vect = R.t()*(cv::Mat_<double>(3, 1) << 0.0, -1.0, 0.0);  // -1 because opencv matrices are 0,0 top right





 	pos_angle = atan2(-zed_pos(2,0), zed_pos(0,0));

	cout << "ZED N°" << x << " results:" << endl;
	cout << std::setprecision(3);
	cout << "       rot_mat: " << formatMat->format( R.t() ) << endl;
	cout << "		zed_pos: " << formatMat->format( zed_pos.t() ) << "   pos angle: " << pos_angle << "rad" << endl;
	cout << "		look_at: " << formatMat->format( look_at.t() ) << endl;
	cout << "		up_vect: " << formatMat->format( up_vect.t() ) << endl;

	return true;
}


class CudaBackgroundSubtractor {

	private:
	cv::cuda::GpuMat _background, _key_mask;
	cv::Mat file_img;
	cv::Ptr<cv::cuda::Filter> _feather;
	int _x; // ZED camera index

	//	cv::cuda::Stream sync = cv::cuda::Stream();

	public:

	CudaBackgroundSubtractor(int x) {
		_x = x;
		file_img = cv::imread("background_image_"+ to_string(_x) +".png");
		_background.upload(file_img);


		_feather = cv::cuda::createBoxMaxFilter(CV_8UC1, cv::Size(30,30));

	}

	void setBackground(cv::cuda::GpuMat& colour ) {

		colour.copyTo(_background);
		colour.download(file_img);
		cv::imwrite("background_image_"+ to_string(_x) +".png", file_img);
	}

	void apply(cv::cuda::GpuMat& image, cv::cuda::GpuMat& mask, int threshold ) {

		if ( _background.empty() || _key_mask.empty() ) {
			_background = cv::cuda::GpuMat( image.size(), CV_8UC4 );
			_key_mask = cv::cuda::GpuMat( image.size(), CV_8UC1 );
		}

		_feather->apply(mask, _key_mask);
		cuThresholdDifference(image, _background, _key_mask, (float)threshold/255.f);
		cv::cuda::bitwise_or(mask, _key_mask, mask);

	}

};

// takes in a GpuMat and downloads it to a cv::Mat used for display
void downloadToDisplay( cv::cuda::GpuMat &image, cv::Mat &display ) {

		cv::cuda::GpuMat image_copied;
		cv::Size display_size_transposed = cv::Size(display_size.height, display_size.width);


		cv::cuda::resize(image, image_copied, display_size_transposed);
		cv::cuda::transpose(image_copied, image_copied);
		cv::cuda::flip(image_copied, image_copied, 1);

		//  copy the shrunk display images
		image_copied.download(display);

}



void zedGrabThread(int x) {
	cout << "ZED N°" << x << " grab thread started. " << endl;

 	//checkCudaErrors(cudaSetDevice(mapZed2GPU(x)));
	cudaSetDevice(mapZed2GPU(x));

	sl::Resolution image_size = zed[x].getResolution();
	cv::Size ocv_size = cv::Size(image_size.width, image_size.height);

	// ZED SDK matrices that will store retrieved data from the ZED cameras
	sl::Mat zed_colour_img(image_size, sl::MAT_TYPE_8U_C4, sl::MEM_GPU);
	sl::Mat zed_depth_meas(image_size, sl::MAT_TYPE_32F_C1, sl::MEM_GPU);
	//sl::Mat zed_point_cloud(image_size, sl::MAT_TYPE_32F_C4, sl::MEM_GPU);

	// OpenCV matrices that are mappped to the sl::mats above
	cv::cuda::GpuMat ocv_colour_img = SLGPUMAT2CVGPUMAT(zed_colour_img, CV_8UC4);
	cv::cuda::GpuMat ocv_depth_meas = SLGPUMAT2CVGPUMAT(zed_depth_meas, CV_32FC1);

	// OpenCV working matrices
	cv::cuda::GpuMat colour_data, depth_data, temp;
	cv::cuda::GpuMat ocv_depth_norm( ocv_size, CV_32FC1), ocv_depth_img(ocv_size, CV_8UC4);
	cv::cuda::GpuMat pos_fullmask( ocv_size, CV_8UC1),  pos_mask( colour_size, CV_8UC1);//, key_mask( colour_size, CV_8UC1);
	//cv::cudev::GpuMat_<float> test(ocv_depth_norm);

	// prepare the overlay
	cv::cuda::GpuMat overlay(calibration_image);
	cv::cuda::resize(overlay, overlay, colour_size);
	cv::cuda::cvtColor(overlay, overlay, CV_BGR2RGBA, 0);


	CudaBackgroundSubtractor bkgd_subtractor(x);

	sl::RuntimeParameters run_params = sl::RuntimeParameters(sl::SENSING_MODE_STANDARD, true, false);


sl::CameraParameters zed_params = zed[x].getCameraInformation().calibration_parameters.left_cam;
glm::mat3 cam_mat = glm::mat3(zed_params.fx, 0, 0,   0, zed_params.fy, 0,   zed_params.cx, zed_params.cy, 1.0);
//glm::mat3 cam_mat = glm::mat3(zed_params.fx, 0, 0,   0, zed_params.fy, 0,   zed_params.cx, ocv_size.height-zed_params.cy, 1.0);

//DepthCamRenderer * rendr;
if( x == 0 ) {
	cv::Size window = cv::Size(1280, 720);
	cv::Size capture_size = cv::Size(image_size.width, image_size.height);



	//rendr = new DepthCamRenderer(window, capture_size, cam_mat);
}


	//, Point anchor=Point(-1, -1), int borderMode=BORDER_DEFAULT, Scalar borderVal=Scalar::all(0))

	while (!stop_signal) {

		bool res = zed[x].grab(run_params);

		if (!res) {
			//cout << "ZED N°" << x << " grabbed images" << endl;
			zed_timestamp[x] = zed[x].getCameraTimestamp();
			auto tp1 = std::chrono::system_clock::now();

			//Grab fresh images from the ZED camera
			zed[x].retrieveMeasure(zed_depth_meas, sl::MEASURE_DEPTH, sl::MEM_GPU);
			//zed[x].retrieveMeasure(zed_point_cloud, sl::MEASURE_XYZ, MEM_GPU);
			auto tp15 = std::chrono::system_clock::now();
			cuNormalizeDepthToUchar4(zed_depth_meas, ocv_depth_img, 2.0, 1.0);
			cuNormalizeDepth(zed_depth_meas, ocv_depth_norm, 0.0, 10.0);

			cuMask3DSpace(zed_depth_meas, pos_fullmask, zed_params, zed_to_world_mat[x]);
			//cuMask3DSpace(zed_point_cloud, mask, zed_to_world_mat[x]);

			auto tp2 = std::chrono::system_clock::now();
			zed[x].retrieveImage(zed_colour_img, sl::VIEW_LEFT, sl::MEM_GPU);
			auto tp3 = std::chrono::system_clock::now();


			if( save_image[x] > 0 ) {
				cv::Mat ocv_save;
				ocv_colour_img.download(ocv_save);
				cv::imwrite("captures/zed"+to_string(x)+"_"+to_string(save_image[x])+"_colour.png", ocv_save);
				ocv_depth_img.download(ocv_save);
				cv::imwrite("captures/zed"+to_string(x)+"_"+to_string(save_image[x])+"_depth.png", ocv_save);
				save_image[x] = -1;
			}



			if( x == 0 ) {

				//cv::Mat temp;
				//ocv_depth_img.download(temp);
				//cv::imshow("test", temp);
				/*rendr->loadDepth(ocv_depth_norm);
				rendr->loadColour(ocv_colour_img);

				rendr->onIdle();
				rendr->render();//*/
			}//*/


			//resize the images - we capture higher resolution for better image quality
			cv::cuda::resize(ocv_colour_img, colour_data, colour_size);
			//cv::cuda::resize(ocv_depth_img, depth_data, colour_size);
			cv::cuda::resize(ocv_depth_img, depth_data, depth_size);
			cv::cuda::resize(pos_fullmask, pos_mask, colour_size);



			if( grab_background ) {

				printZedSettings(x);
				// save current image as the background
				bkgd_subtractor.setBackground( colour_data );

				//pMOG2->apply(colour_data, key_mask, 1.0);

			} else if(be_removing_background) {

				//bkgd_subtractor.apply( colour_data, pos_mask, bkgd_thresh );
				//pMOG2->apply(colour_data, key_mask, 0.000);

				//depth_data.setTo(cv::Scalar(0.0), key_mask);
				colour_data.setTo(cv::Scalar(0.0), pos_mask);
			}

			//cv::cuda::resize(depth_data, depth_data, depth_size);


			if( be_updating_data) {
				depth_data.download(depth_result[x]);
				colour_data.download(colour_result[x]);
			}

			if(! be_removing_background ) {
				//colour_data.setTo(cv::Scalar(0.0), pos_mask);
				cv::cuda::add(colour_data, overlay, colour_data);
			}

			downloadToDisplay(colour_data, colour_display[x]);
			downloadToDisplay(depth_data, depth_display[x]);

			auto tp4 = std::chrono::system_clock::now();

			//cout << "ZED N°" << x << " grab: " << TO_MS(tp2 - tp1) 	<< "ms, depth: " <<TO_MS(tp3 - tp2)	<< "ms, proc: " <<TO_MS(tp4 - tp3) << "ms \n";

		}
		this_thread::sleep_for(milliseconds(1));
	}
	//delete zed[x];
	zed_depth_meas.free(sl::MEM_GPU);
	zed_colour_img.free(sl::MEM_GPU);
	zed[x].close();
}



void remap_window_segments( vector<int> order ) {
	//Create windows for viewing results with OpenCV
	for (int i = 0; i < NUM_CAMERAS; i++) {

		colour_display[order[i]] = zed_display(cv::Rect(i*display_size.width, 0, display_size.width, display_size.height));
		depth_display[order[i]] = zed_display(cv::Rect(i*display_size.width, display_size.height, display_size.width, display_size.height));
	}

}

#define CVMAT2GLMVEC3(mat) glm::vec3(mat.at<float>(0,0), mat.at<float>(1,0), mat.at<float>(2,0))

void calibrateAllZeds(vector<int> &zed_order, UDPSenderReceiver &udpSR, bool load_saved = false ) {
	// calibrate each zed camera
	int found = 0;
	cv::Mat_<float> zed_pos[3], look_at[3], up_vect[3];
	float zed_pos_angle[3];

	if( load_saved ) {
		// load saved calibration
		cv::FileStorage fs = cv::FileStorage("zed_world_calibration.yml", cv::FileStorage::READ);
		for (int i = 0; i < NUM_CAMERAS; i++) {
			fs["zed_pos_"+to_string(i)] >> zed_pos[i];
			fs["look_at_"+to_string(i)] >> look_at[i];
			fs["up_vect_"+to_string(i)] >> up_vect[i];
			fs["zed_pos_angle_"+to_string(i)] >> zed_pos_angle[i];
		}
		fs["cameras_saved"] >> found;
		fs.release();

	} else {
		// calibrate each camera
		for (int i = 0; i < NUM_CAMERAS; i++)
			found += (int) calibrateZedToWorld(i, zed_pos[i], look_at[i], up_vect[i], zed_pos_angle[i]);

		if( found == NUM_CAMERAS ) {
			// store the calibration that was just measured
			cv::FileStorage fs = cv::FileStorage("zed_world_calibration.yml", cv::FileStorage::WRITE);
			fs << "cameras_saved" << found;
			for (int i = 0; i < NUM_CAMERAS; i++) {
				fs << "zed_pos_"+to_string(i) << zed_pos[i];
				fs << "look_at_"+to_string(i) << look_at[i];
				fs << "up_vect_"+to_string(i) << up_vect[i];
				fs << "zed_pos_angle_"+to_string(i) << zed_pos_angle[i];
			}
			fs.release();
		}
	}

	// useful debug output
	cout << std::setprecision(3);
	for (int i = 0; i < NUM_CAMERAS; i++)
		cout << "zed pos angle " << i << ": " <<  zed_pos_angle[i] << endl;

	// only send update if all cameras we calibrated
	if( found == NUM_CAMERAS ) {
		// sort zed cameras by angle and remap
		iota(zed_order.begin(), zed_order.end(), 0);
		sort(zed_order.begin(), zed_order.end(),
			[&zed_pos_angle](size_t i1, size_t i2) {return zed_pos_angle[i1] < zed_pos_angle[i2];});
		cout << " new order of zed cameras: " << zed_order[0] << ">" << zed_order[1] << ">" << zed_order[2] << endl;
		remap_window_segments( zed_order );

		cout << " sending zed positions " << endl;
		for (int i = 0; i < NUM_CAMERAS; i++) {
			udpSR.sendZedPositions( zed_pos[zed_order[i]], look_at[zed_order[i]], up_vect[zed_order[i]], i );
			// save a mat4 used in depth extraction
			zed_to_world_mat[i] = glm::inverse( glm::lookAt(CVMAT2GLMVEC3(zed_pos[i]), CVMAT2GLMVEC3(look_at[i]), CVMAT2GLMVEC3(up_vect[i])) );

			//cout << "sanity check zed[" << i << "] is looking at " << to_string( zed_to_world_mat[i] * glm::vec4(0.0f,0.5f,-1.5f,1.0f) ) << endl;
			cout << "glm::zed_to_world_mat " << to_string(zed_to_world_mat[i]) <<  endl;
		}
	}

}
//main  function
int main(int argc, char** argv) {

	cout << "Found: OpenCV version " << CV_VERSION << endl;
	// print all floating floating point numbers with 2 decimal points
	cout << setiosflags(ios::fixed) << setprecision(1) ;

	sl::InitParameters init_params;
	init_params.camera_resolution = ZED_RES;
	init_params.camera_fps = FPS;
	init_params.depth_mode = sl::DEPTH_MODE_QUALITY;
	init_params.coordinate_units = sl::UNIT_METER;
	init_params.depth_minimum_distance = depth_min;
	init_params.sdk_verbose = true;
	//init_params.camera_buffer_count_linux = 16;


	// Initialize ZED cameras
	for (int i = 0; i < NUM_CAMERAS; i++) {

		cout << "ZED N°" << i << " -> Initializing camera id: " << i << endl;
		//zed[i] = new Camera();//ZED_RES, FPS, i);

		init_params.camera_linux_id = i;
		init_params.sdk_gpu_id = mapZed2GPU(i);

		sl::ERROR_CODE err = zed[i].open(init_params);
		

		cout << "ZED N°" << i << " -> Result : " << sl::errorCode2str(err) << endl;
		if (err != sl::SUCCESS) {
			zed[i].close();
			return 1;
		}
		
		//zed[i].enableTracking(); //apparently tracking has to be enabled for the depth sensing stabilization

		depth_min = zed[i].getDepthMinRangeValue();
		cout << "depth min " << depth_min << endl;
		colour_result[i] = cv::Mat(colour_size, CV_8UC4);
		depth_result[i] = cv::Mat(depth_size,  CV_8UC1);// CV_16UC1);

	}

	// Set the camera settings such as gain and exposure
	autoAdjustZedSettings();

	// Create and map the viewing window
	vector<int> zed_order( NUM_CAMERAS );
	iota(zed_order.begin(), zed_order.end(), 0);

	zed_display = cv::Mat(display_size.height*2, display_size.width*NUM_CAMERAS, CV_8UC4);
 	remap_window_segments( zed_order ); // this is redone later in calibrateAllZeds



	//Create both grabbing thread with the camera number as parameters
	vector<thread*> thread_vec;
	for (int i = 0; i < NUM_CAMERAS; i++)
		thread_vec.push_back(new thread(zedGrabThread, i));


	// Initialize compression paramaters
	vector<int> compression_params = {CV_IMWRITE_PNG_COMPRESSION, 3, CV_IMWRITE_JPEG_QUALITY, 97};

	// Initialize the UDP socket
	UDPSenderReceiver udpSR(200000);
	if( !udpSR.connectOutSocket() ) { return -1; }

	//cout << (long) &listeners << " port " << listeners.sin_port << endl;

	// Load saved ZED camera world calibration
	calibrateAllZeds(zed_order, udpSR, true );


	char key = ' ';
	long frame_id = 0, capture_id = 0;

	// temporary variables that can't be declared inside the switch case
	unsigned char odroid_id = 0;
	float x_off = 0.0, y_off = 0.0, r_off = 0.0;


	// fake zed position used of manual calibration
	cv::Mat zp = (cv::Mat_<float>(3, 1) << 1.5, 1.0, 0.0);
	cv::Mat za = (cv::Mat_<float>(3, 1) << 0.0, 1.0, 0.0);
	cv::Mat zu = (cv::Mat_<float>(3, 1) << 0.0, 0.0, -1.0);

	bool show_calibration = false;
	//loop until 'q' is pressed
	while (key != 'q') {
		auto start_tp = std::chrono::system_clock::now();



		//if(key > 0) cout << key << " : " << (int) key << endl;
		grab_background = ( key == 'b' );
		switch( key ) {
			case 'a':
				autoAdjustZedSettings();
				break;
			case '-':
			case -83:
				bkgd_thresh--;
				cout << " decreased threshold to: " << bkgd_thresh << endl;
				break;
			case '+':
			case -85:
				bkgd_thresh++;
				cout << " increased threshold to: " << bkgd_thresh << endl;
				break;
			case 's':
				be_sending_data = ! be_sending_data;
				cout << " sending data turned " << (be_sending_data ? "on" : "off") << endl;
				break;
			case 'r':
				be_removing_background = ! be_removing_background;
				cout << " background removal turned " << (be_removing_background ? "on" : "off") << endl;
				break;
			case 'p':
				be_updating_data = ! be_updating_data;
				cout << " updating data " << (be_updating_data ? "on" : "off") << endl;
				break;
			case 'i':
				cout << " saving images #" << ++capture_id << "into the 'captures' directory" << endl;

				mkdir("captures", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				for (int i = 0; i < NUM_CAMERAS; i++) {
					save_image[i] = capture_id;
					//cv::imwrite("captures/zed"+to_string(i)+"_"+to_string(capture_id)+".png", colour_result[i], compression_params);
				}
				break;
			case 'c':
				show_calibration = !show_calibration;
				cout << " manual projector calibration mode "  << (show_calibration ? "on" : "off") <<  endl;

				//cout << " sending fake zed positions " << endl;
				//for (int i = 0; i < NUM_CAMERAS; i++) udpSR.sendZedPositions( zp, za, zu, i );
				break;
			case 'z':
				cout << " calibrating home position " << endl;
 				calibrateAllZeds(zed_order, udpSR );
				break;
			case 'P':	//home, previous odroid
				odroid_id--;//odroid id is negative
				cout << " selected odroid: " << (int) odroid_id << endl;
				break;
			case 'W':	//end, next odroid
				odroid_id++;//odroid id is negative
				cout << " selected odroid: " << (int) odroid_id << endl;
				break;
			case 'R':	//up arrow, move the model upwards
				cout << " moving up" << endl;
				x_off = -0.0025;
				break;
			case 'T':	//down arrow, move the model downwards
				cout << " moving down" << endl;
				x_off = 0.0025;
				break;
			case 'Q':	//left arrow, move the model to the left
				cout << " moving left" << endl;
				y_off = - 0.0025;
				break;
			case 'S':	//right arrow, move the model to the right
				cout << " moving right" << endl;
				y_off = + 0.0025;
				break;
			case 'U':	// pageUp - rotate projector CCW
				cout << " rotating CCW" << endl;
				r_off = - 0.0025;
				break;
			case 'V':	// pageDown - rotate projector CW
				cout << " rotating CW" << endl;
				r_off = + 0.0025;
				break;

		}


		if (  be_sending_data ) {


				if(show_calibration) {
					// do this every update as long as we're calibrating
					//cout << "sent offset" <<to_string( x_off )<< " " << to_string(y_off) << endl;
					udpSR.sendManualOffsets( odroid_id, x_off, y_off, r_off );
					x_off = 0.0;
					y_off = 0.0;
					r_off = 0.0;
				}


				for (int i = 0; i < NUM_CAMERAS; i++) {
					//	udpSR.sendFrame(calibration_image, calibration_depth, compression_params, i, frame_id );
					udpSR.sendFrame(colour_result[zed_order[i]], depth_result[zed_order[i]], compression_params, i, frame_id );
				}
				frame_id ++;

		}

		cv::imshow("ZED Telehuman Server", zed_display);

		// Sleep to match OUT_FPS
		duration<double, milli> dur = system_clock::now() - start_tp;
		int miliseconds_left = (int) (1000.0/OUT_FPS - dur.count());
		if(  miliseconds_left > 0 ) {
			this_thread::sleep_for(milliseconds(miliseconds_left));
			dur = duration_cast<microseconds>( system_clock::now() - start_tp);
			//cout << "slept for " << miliseconds_left << "ms, total cycle " << dur.count() << "ms"<< endl;
		} else {
			cout << "main loop took " <<  dur.count() << "ms which is too slow for the set output FPS of " << OUT_FPS << endl;
		}
		//this_thread::sleep_for(milliseconds(1000/FPS));
		//compare Timestamp between both camera (uncomment following line)
		//for (int i = 0; i < NUM_CAMERAS; i++) cout << " Timestamp " << i << ": " << zed_timestamp[i] << endl;


		//get the Key
		key = cv::waitKey(1);
	}

	//out --> tells both thread to finish
	stop_signal = true;

	//end of thread --sync
	for (auto it : thread_vec) it->join();
		cout << "All threads exited. " << endl;
	return 0;
}
