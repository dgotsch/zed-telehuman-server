#version 450
#define mediump

in vec3 coord3d;
in vec2 texcoord;
out vec2 f_texcoord;
out vec3 v_coord;
out float do_draw;
uniform sampler2D deptexture;
uniform mat4 mvp;
uniform vec3 zed;

void main(void) {
	vec3 dir = coord3d- vec3(0,0,0); //assume we are in zed coordinates
	dir = dir / dir.z; // the range of depth is 1 m
	
	ivec2 size = textureSize(deptexture, 0); // depth texture resolution
	ivec2 itexcoord = ivec2(texcoord * size);
	float dist = texelFetch(deptexture, itexcoord, 0).r*10.0; // distance from camera in meters
	
	
	do_draw = float(dist > 0.5); // ignore things closer than 0.5m since they are 'not found' values
	vec3 with_depth = coord3d + dir * dist;

	gl_Position = mvp * vec4(with_depth, 1.0);
	v_coord = (mvp * vec4(vec3(0), 1.0)).xyz;
	f_texcoord = texcoord;
}
