/*
Copyright (c) 2017 Daniel Gotsch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// The code below is a heavily modified version of OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
*/


/**
 * From the OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */


#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
/* Use glew.h instead of gl.h to get all the GL prototypes declared */
#include <GL/glew.h>
/* Using the GLUT library for the base windowing setup */
#include <GL/freeglut.h>
/* GLM */
// #define GLM_MESSAGES
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "opengl-common/shader_utils.h"

#include "mesh_generator.cpp"

//opencv includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <opencv2/cudaimgproc.hpp>


/*
// cuda includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

*/

//ZED Includes
#include <sl/Camera.hpp>

using namespace glm;




class DepthCamRenderer {
	public:
	cudaGraphicsResource_t col_cuda_res, dep_cuda_res;

	private:
	GLuint vbo_combined;
	GLuint program;
	GLuint col_texture_id, dep_texture_id, out_texture_id;
	GLuint fbo_output, rbo_depth_output;
	GLint attribute_coord3d, attribute_texcoord;
	GLint uniform_mvp, uniform_zed, uniform_coltexture, uniform_deptexture;

	int vertex_count;

	cv::Size window;

	cv::ogl::Texture2D col_ogl_tex, dep_ogl_tex;

	// a helper function that loads up a texture into OpenGL
	void init_texture( cudaGraphicsResource_t &resource, cv::Size image_size, GLuint& texture_id, GLint internalFormat, GLenum format  ) {


		glGenTextures(1, &texture_id);
		glBindTexture(GL_TEXTURE_2D, texture_id);
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					//set texture clamping method
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(GL_TEXTURE_2D, // target
						 0,	// level, 0 = base, no minimap,
						 internalFormat, // internalFormat
						 image_size.width,	// width
						 image_size.height,	// height
						 0,	// border, always 0 in OpenGL ES
						 format,	// format
						 GL_UNSIGNED_BYTE, // type
						 NULL);
						 //mat_texture.data);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
		//cudaGraphicsGLRegisterImage(&resource, texture_id, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard);

	}

	public:
	DepthCamRenderer(cv::Size & window, cv::Size & capture_size, mat3 & cam_mat)
	{
		// create a window
		int argc = 0;
		glutInit(&argc, NULL);
		glutInitContextVersion(2,0);
		glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
		glutInitWindowSize(window.width, window.height);
		glutCreateWindow("My Textured Cube");

		this->window = window;

		GLenum glew_status = glewInit();
		if (glew_status != GLEW_OK) {
			fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
			exit(1);
		}

		// generate a triangle mesh
		float min_z = 1.0;
		vec3 bl = glm::inverse(cam_mat) * vec3(0.0, 0.0, min_z);
		vec3 ur = glm::inverse(cam_mat) * vec3(capture_size.width, capture_size.height, min_z);

		cout << "bl : " << bl.x << ", " << bl.y << ", " << bl.z << endl;
		cout << "ur : " << ur.x << ", " << ur.y << ", " << ur.z << endl;

		TriangleMeshGenerator gen( bl.x , bl.y , ur.x , ur.y , min_z, 1280, 720);
		vector<float>& vertices = gen.getMesh();


		// load the triangle mesh
		glGenBuffers(1, &vbo_combined);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_combined);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW);



		// load the color texture in
		//cv::Mat col_texture = cv::imread("../src/test_colour.jpg", CV_LOAD_IMAGE_COLOR);
		glGenTextures(1, &col_texture_id);
		col_ogl_tex = cv::ogl::Texture2D(capture_size, cv::ogl::Texture2D::Format::RGB, true);
		glGenTextures(1, &dep_texture_id);
		// TODO: this might be faster if loaded as a texture buffer, see
		//https://www.khronos.org/opengl/wiki/Buffer_Texture
		//https://gist.github.com/roxlu/5090067
		dep_ogl_tex = cv::ogl::Texture2D(capture_size, cv::ogl::Texture2D::Format::DEPTH_COMPONENT, true);
		//init_texture( col_cuda_res, capture_size, col_texture_id, GL_RGB, GL_BGR);
		//cv::ogl::Texture2D tex;
			// see http://stackoverflow.com/questions/18086519/is-it-possible-to-bind-a-opencv-gpumat-as-an-opengl-texture



	
		// compile and load the shaders
		program = create_program("../src/mesh.v.glsl", "../src/mesh.f.glsl");
		if (program == 0) {
			fprintf(stderr, "Error: Could not create GLSL program:\n");
			exit(1);
		}

		/*
		// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
		glGenFramebuffers(1, &fbo_output);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo_output);

		// The texture we're going to render to
		glGenTextures(1, &out_texture_id);
		// "Bind" the newly created texture : all future texture functions will modify this texture
		glBindTexture(GL_TEXTURE_2D, out_texture_id);
		// Give an empty image to OpenGL ( the last "0" )
		glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, screen_width, screen_height, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);
		// Poor filtering. Needed !
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// The depth buffer
		glGenRenderbuffers(1, &rbo_depth_output);
		glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_output);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screen_width, screen_height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo_depth_output);

		// Set "out_texture_id" as our colour attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, out_texture_id, 0);

		// Set the list of draw buffers.
		GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
		glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

		// Always check that our framebuffer is ok
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			return 0;


	//*/

		// grab the rest of the variables
		attribute_coord3d = get_attrib(program, "coord3d");
		attribute_texcoord = get_attrib(program, "texcoord");

		uniform_mvp = get_uniform(program, "mvp");
		//if( (uniform_zed = get_uniform(program, "zed")) == -1 ) return 0;
		uniform_coltexture = get_uniform(program, "coltexture");
		uniform_deptexture = get_uniform(program, "deptexture");



		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		//glEnable(GL_CULL_FACE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glClearColor(1.0, 1.0, 1.0, 1.0);


		glUseProgram(program);

		// asign the texture uniforms to actual textures
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(uniform_coltexture, /*GL_TEXTURE*/0);
		glActiveTexture(GL_TEXTURE1);
		glUniform1i(uniform_deptexture, /*GL_TEXTURE*/1);

		// load in the geometry
		glBindBuffer(GL_ARRAY_BUFFER, vbo_combined);
		glEnableVertexAttribArray(attribute_coord3d);
		glVertexAttribPointer(attribute_coord3d, 3, GL_FLOAT,GL_FALSE, 5*sizeof(GLfloat), 0 );

		glEnableVertexAttribArray(attribute_texcoord);
		glVertexAttribPointer(attribute_texcoord,2,GL_FLOAT,GL_FALSE, 5*sizeof(GLfloat), (GLvoid*) (3*sizeof(GLfloat)) );//*/


		glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &vertex_count);
	}

	~DepthCamRenderer() {
		glDeleteProgram(program);
		glDeleteBuffers(1, &vbo_combined);
		glDeleteTextures(1, &col_texture_id);
		glDeleteTextures(1, &dep_texture_id);
	}

	void onIdle() {
		mat4 reCenter = translate(mat4(1.0f), vec3(0.0, 0.0, -1.5));
		float angle = glutGet(GLUT_ELAPSED_TIME) / 1000.0 * glm::radians(15.0);	// base 15° per second
		mat4 anim = \
			glm::rotate(mat4(1.0f), angle*3.0f, vec3(1, 0, 0)) *	// X axis
			glm::rotate(mat4(1.0f), angle*0.0f, vec3(0, 1, 0)) *	// Y axis
			glm::rotate(mat4(1.0f), angle*0.0f, vec3(0, 0, 1));	 // Z axis

		mat4 model = glm::translate(mat4(1.0f), vec3(0.0, 0.0, -2.0));
		mat4 view = glm::lookAt(vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, -2.0), vec3(0.0, 1.0, 0.0));
		mat4 projection = glm::perspective(45.0f, 1.0f*window.width/window.height, 0.1f, 10.0f);


		glm::mat4 mvp = projection * view * model * anim * reCenter;
		glUseProgram(program);
		glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
		glutPostRedisplay();
	}


	void loadColour( cv::cuda::GpuMat & image ) {
		// load the color texture in

		glActiveTexture(GL_TEXTURE0);
		{
			col_ogl_tex.copyFrom(image);
			col_ogl_tex.bind();
		}



		/*
	    cudaGraphicsMapResources(1, &col_cuda_res);
		{
		    cudaArray *image_cuda;
		    cudaGraphicsSubResourceGetMappedArray(&image_cuda, col_cuda_res, 0, 0);
		    cudaMemcpy2D( surface_obj, image.getPtr<void>(sl::MEM_GPU)
		    cudaResourceDesc img_res_desc;
		    {
		        img_res_desc.resType = cudaResourceTypeArray;
		        img_res_desc.res.array.array = image_cuda;
		    }
		    cudaSurfaceObject_t surface_obj;
		    cudaCreateSurfaceObject(&surface_obj, &img_res_desc);
		    {
		    	cudaMemcpy2D( surface_obj, image.getPtr<void>(sl::MEM_GPU)
		        //invokeRenderingKernel(surface_obj);
		    }
		    cudaDestroySurfaceObject(surface_obj));
		}
		cudaGraphicsUnmapResources(1, &col_cuda_res);

		cudaStreamSynchronize(0);*/

		//init_texture( image, col_texture_id, GL_RGB, GL_BGR);


	}

	void loadDepth(  cv::cuda::GpuMat & image ) {

		glActiveTexture(GL_TEXTURE1);
		{
			dep_ogl_tex.copyFrom(image);
			dep_ogl_tex.bind();
		}
	}

	void render()
	{
		//std::cout << "onDisplay()" << endl;

		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);




		/* Push each element in buffer_vertices to the vertex shader */
		glDrawArrays(GL_TRIANGLE_STRIP, 0, vertex_count/(5*sizeof(GLfloat)) );


		//glDisableVertexAttribArray(attribute_coord3d);
		//glDisableVertexAttribArray(attribute_texcoord);

		//glBindFramebuffer(GL_FRAMEBUFFER, fbo_output);

		glutSwapBuffers();
	}

	void onReshape(int width, int height) {
		window.width = width;
		window.height = height;
		glViewport(0, 0, width, height);
	}

};



/*
int main(int argc, char* argv[]) {

	cv::Size window = cv::Size(1280, 720);
	cv::Size capture_size = cv::Size( 2208, 1242 );
	glm::mat3 cam_mat = glm::mat3(1361.200073242188, 0, 0,
								 0, 1361.200073242188, 0,
								 1102.0, 621.0, 1.0);
	DepthCamRenderer rendr = DepthCamRenderer(window, capture_size, cam_mat);


	/*
	glutDisplayFunc(onDisplay);
	glutReshapeFunc(onReshape);
	glutIdleFunc(onIdle);
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glutMainLoop();

	onDisplay();
	*/
		/*

	while(true) {

		rendr.onIdle();
		rendr.render();


	}
	return 0;

}
// */
