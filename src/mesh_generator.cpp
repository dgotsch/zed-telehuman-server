#include <vector>
using namespace std;

class TriangleMeshGenerator 
{
	private: 
	float xStep, yStep, xFrac, yFrac;
	float xMin, yMin, zVal;
	vector<float> vertices;
	public: const int entrySize = 5;

	//TriangleMeshGenerator(float, float, float, float, int, int, vector<float>& ) {};
	private:
	void addVertex( int& index, int x, int y) 
	{
		vertices[index++] = xMin + (float)x * xStep;	//x
		vertices[index++] = yMin + (float)y * yStep;	//y
		vertices[index++] = zVal; //z
		
		vertices[index++] = (float)x * xFrac; //x
		vertices[index++] = (float)y * yFrac; //y 
	}
	
	public:
	vector<float>& getMesh() 
	{
		return vertices;
	}
	
	int meshSize ()
	{
		return vertices.size()/ entrySize;
	}

	TriangleMeshGenerator( float xMin, float yMin, float xMax, float yMax, float zVal, int xCount, int yCount) 
	{
		this->xMin = xMin;
		this->yMin = yMin;
		this->zVal = zVal;
		xFrac = 1.0 / (float) (xCount);
		yFrac = 1.0 / (float) (yCount);
		xStep = (xMax - xMin) *xFrac;
		yStep = (yMax - yMin) *yFrac;
		
		int index = 0;
		
		bool extraVertex = (yCount&1) == 0;
		// Resize the output vectors
		int vertexCount = 2*(xCount)*yCount + 2*((yCount+1)/2) + (int) extraVertex; // even rows have 2 extra vertices
		vertices.resize(vertexCount * entrySize); 
	
		// Create the vertices and texture coordinates
		for(  int y = 0; y < yCount; ++y )
		{
			if( (y&1) == 0 )	//even rows
			{
				for( int x = 0; x <= xCount; ++x )
				{
					addVertex(index, x, y);
					addVertex(index, x, y+1);
				}
			}
			else//odd rows
			{
				for( int x = xCount; x > 0; --x )	//lacks the bottom left node
				{
					addVertex(index, x, y+1);
					addVertex(index, x-1, y);
				}
			}
			
		}
		if( extraVertex )//have even rows, add the top left node
		{
			addVertex(index, 0, yCount);
		}
		
//		cout << " we made  " << index/entrySize << " vertices and expected to make " << vertexCount << endl;
		
	}	
	
};

// THIS IS HOW TO USE THE GENERATOR:



    // TriangleMeshGenerator gen( -img_w_hf * img_asp , -img_w_hf , img_w_hf * img_asp , img_w_hf , triangleCol, triangleRow);
    //vector<float>& vertices = gen.getMesh();


	/*GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo_id[0]));
	GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW));
	GL_CHECK(glVertexAttribPointer(iLocPosition, 3, GL_FLOAT,GL_FALSE, 5*sizeof(GLfloat), 0 ));
	GL_CHECK(glEnableVertexAttribArray(iLocPosition));
	GL_CHECK(glVertexAttribPointer(iLocTexCoords,2,GL_FLOAT,GL_FALSE, 5*sizeof(GLfloat), (GLvoid*) (3*sizeof(GLfloat)) ));
	GL_CHECK(glEnableVertexAttribArray(iLocTexCoords));
	GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));//*/
	
