#include "kernel.cuh"

using namespace glm;

// TODO: maybe cv::cudev::GpuMat_<T> could be used in this file to replace more generic GpuMats?


template <class T>
struct cuMat {
	T* ptr;
	size_t width;
	size_t height;
	size_t step;

	cuMat ( sl::Mat & mat ) {
		ptr = (T*) mat.getPtr<sl::uchar1>(sl::MEM_GPU);
		width = mat.getWidth();
		height = mat.getHeight();
		step = mat.getStep(sl::MEM_GPU);
	}
	
	cuMat ( cv::cuda::GpuMat & mat ) {
		ptr = mat.ptr<T>();
		width = mat.cols;
		height = mat.rows;
		step = mat.step / mat.elemSize();
	}
	/*
	template <class S>
	cuMat ( cv::cudev::GpuMat_<S> & mat ) {
		//ptr = mat.ptr<T>();
		width = mat.cols;
		height = mat.rows;
		step = mat.step1();
	}
	*/
	
	//bool isInBounds( size_t x, size_t y ) { return x < width && y < height; }
	
	//	  T& At( size_t x, size_t y ) 		{ return ptr[y * step + x]; }
	//const T& At( size_t x, size_t y ) const { return ptr[y * step + x]; }
};
	

#define X_IDX (blockIdx.x * blockDim.x + threadIdx.x)
#define Y_IDX (blockIdx.y * blockDim.y + threadIdx.y)

template <class T>
inline __device__ T& __MatCurrAt( cuMat<T> &mat ) {
	return mat.ptr[Y_IDX * mat.step + X_IDX]; 
}

template <class T>
inline __device__ bool __MatOutOfBounds( cuMat<T> &mat ) {
	// check if it's within bounds
	return (X_IDX >= mat.width && Y_IDX >= mat.height);
}



inline __device__ uchar4 __float4_to_uchar4(float4 source) {

	return make_uchar4( (uchar) 255.f * source.x, 
						(uchar) 255.f * source.y,
						(uchar) 255.f * source.z,
						(uchar) 255.f * source.w );
}

inline __device__ float4 __uchar4_to_float4(uchar4 source) {

	return make_float4( (float) source.x / 255.f, 
						(float) source.y / 255.f,
						(float) source.z / 255.f,
						(float) source.w / 255.f);
}

inline __device__ vec4 __float4_to_vec4(float4 source) {
	return vec4( source.x, source.y, source.z, source.w);
}


inline __device__ float4 __RGB_to_YCbCr(float4 source) {
// values from https://en.wikipedia.org/wiki/YCbCr
	return make_float4( 0.0 + 0.299		* source.x + 0.587		* source.y + 0.114		* source.z,
						0.5 - 0.168736	* source.x - 0.331264	* source.y + 0.5		* source.z,
						0.5 + 0.5		* source.x - 0.418688	* source.y - 0.081312	* source.z,
						source.w);
}


__global__ void _normalize(  cuMat<float> depth_meas, cuMat<float> depth_norm, float min_val, float range) {
	if( __MatOutOfBounds(depth_meas) || __MatOutOfBounds(depth_norm) ) return;
	
  	float norm_depth = (__MatCurrAt(depth_meas)  - min_val)/ range;
	__MatCurrAt(depth_norm) = norm_depth;
	
}

__global__ void _normalize2(  cuMat<float> depth_meas, cuMat<uchar4> depth_norm, float min_val, float range) {
	if( __MatOutOfBounds(depth_meas) || __MatOutOfBounds(depth_norm) ) return;
	
  	float norm_depth = (__MatCurrAt(depth_meas)  - min_val)/ range;
  	uchar tmp = (uchar) 255.f * norm_depth;
	__MatCurrAt(depth_norm) = make_uchar4(tmp, tmp, tmp, 255);
	//__float4_to_uchar4(make_float4( norm_depth, norm_depth, norm_depth, 1.0 ));
}

void cuNormalizeDepth(sl::Mat &depth_meas, cv::cuda::GpuMat &depth_norm, float min_val, float max_val) {
	// get the image size
	size_t width = depth_meas.getWidth();
	size_t height = depth_meas.getHeight();

	// define block dimensions
	dim3 block(32,8);
	dim3 grid(ceill(width / (float)block.x), ceill(height / (float)block.y));

	//struct timeval timerStart;
	//gettimeofday(&timerStart, NULL);
	// call the kernel
	_normalize <<<grid, block >>>( cuMat<float>(depth_meas), cuMat<float>(depth_norm), min_val, max_val - min_val);
	//cudaThreadSynchronize();
	//struct timeval timerStop, timerElapsed;
    //gettimeofday(&timerStop, NULL);
    //timersub(&timerStop, &timerStart, &timerElapsed);
    //double ms = timerElapsed.tv_sec*1000.0+timerElapsed.tv_usec/1000.0;
	//printf ("Time for the kernel: %f ms\n", ms);
}


void cuNormalizeDepthToUchar4(sl::Mat &depth_meas, cv::cuda::GpuMat &depth_norm, float min_val, float max_val) {
	// get the image size
	size_t width = depth_meas.getWidth();
	size_t height = depth_meas.getHeight();

	// define block dimensions
	dim3 block(32,8);
	dim3 grid(ceill(width / (float)block.x), ceill(height / (float)block.y));

	_normalize2 <<<grid, block >>>( cuMat<float>(depth_meas), cuMat<uchar4>(depth_norm), min_val, max_val - min_val);
}







__global__ void _mask3DSpace(  cuMat<float> depth, cuMat<uchar> mask, sl::CameraParameters params, mat4 cam_to_world) {
	if( __MatOutOfBounds(depth) || __MatOutOfBounds(mask) ) return;
	
	//vec3 p; = pix_to_cam * vec3( (float) X_IDX, (float) Y_IDX, 1.0f );
	//p = p *  __MatCurrAt(depth);
	vec3 p;
	p.z = __MatCurrAt(depth);
	p.x = (((float) X_IDX - params.cx) * p.z ) / params.fx;
	p.y = (((float) Y_IDX - params.cy) * p.z ) / params.fy;
	//p = p / p.z * length(p);
	vec4 p_zed = vec4( p, 1.0f);
	
	p_zed.z = - p_zed.z;
	p_zed.y = - p_zed.y;
	
	vec4 p_world = cam_to_world * p_zed;
	/*
	vec4 origin = cam_to_world * vec4(0.f, 0.f, 0.f, 1.f);
	vec3 dir = mat3(cam_to_world) * vec3(p_zed);
	dir = dir * (-origin.y)/ dir.y;
	
	p_world = origin + vec4(dir, 1.f);//*/
	
	float r_squared = p_world.x*p_world.x + p_world.z * p_world.z;
	//if( (p_zed.x > 0.0f || p_zed.y > 0.0f) )  __MatCurrAt(mask) = 0;//&& p_zed.z > -1.5f
	//if( p_world.x > 0.0f || p_world.z < 0.0f)  __MatCurrAt(mask) = 0;
	if( r_squared < 0.25f )  __MatCurrAt(mask) = 0;//&& p_world.y > 0.04
	else __MatCurrAt(mask) = 255;
	
	
}


void cuMask3DSpace(sl::Mat &points, cv::cuda::GpuMat &mask, sl::CameraParameters params, mat4 cam_to_world ) {
	// get the image size
	size_t width = points.getWidth();
	size_t height = points.getHeight();

	// define block dimensions
	dim3 block(32,8);
	dim3 grid(ceill(width / (float)block.x), ceill(height / (float)block.y));
	
	_mask3DSpace <<<grid, block >>>( cuMat<float>(points), cuMat<uchar>(mask), params,  cam_to_world);

}


__global__ void _mask3DSpace(  cuMat<float4> points, cuMat<uchar> mask, mat4 to_world) {
	if( __MatOutOfBounds(points) || __MatOutOfBounds(mask) ) return;
	
	
	vec4 p_zed = __float4_to_vec4(__MatCurrAt(points));
	//p_zed.x = p_zed.x-0.06;
	p_zed.w = 1.0f;
	p_zed.z = - p_zed.z;
	p_zed.y = - p_zed.y;
	
	vec4 p_world = to_world * p_zed;
	
	if( p_world.x > 0.0f || p_world.z > 0.0f) __MatCurrAt(mask) = 0;
	else __MatCurrAt(mask) = 255;
	
	
}



void cuMask3DSpace(sl::Mat &points, cv::cuda::GpuMat &mask, mat4 to_world ) {
	// get the image size
	size_t width = points.getWidth();
	size_t height = points.getHeight();

	// define block dimensions
	dim3 block(32,8);
	dim3 grid(ceill(width / (float)block.x), ceill(height / (float)block.y));
	
	_mask3DSpace <<<grid, block >>>( cuMat<float4>(points), cuMat<uchar>(mask), to_world);

}


__global__ void _thresholdDiff(cuMat<uchar4>  image1, cuMat<uchar4> image2, cuMat<uchar> mask, float colour_thresh)
{
	if( __MatOutOfBounds(image1) || __MatOutOfBounds(image2) || __MatOutOfBounds(mask) ) return;
	
	if(__MatCurrAt(mask) == 0) return;
	
	vec4 u = __float4_to_vec4(__RGB_to_YCbCr(__uchar4_to_float4(__MatCurrAt(image1))));
	vec4 v = __float4_to_vec4(__RGB_to_YCbCr(__uchar4_to_float4(__MatCurrAt(image2))));

	// brightness changes a lot with shadows so we consider it less important
	u.x *= 0.5;
	v.x *= 0.5;
	float len = length(u - v);//(a.x - b.x, a.y - b.y, a.z - b.z); 

	__MatCurrAt(mask) =(( len < colour_thresh ) ? 255 : 0);
	
} 


void cuThresholdDifference(cv::cuda::GpuMat &image1, cv::cuda::GpuMat &image2, cv::cuda::GpuMat &mask, float colour_thresh) {
	
	// get the image size
	size_t width = image1.cols;
	size_t height = image1.rows;

	// define block dimensions
	dim3 block(32,8);
	dim3 grid(ceill(width / (float)block.x), ceill(height / (float)block.y));

	// call the kernel
	_thresholdDiff <<<grid, block >>>(cuMat<uchar4>(image1), cuMat<uchar4>(image2), cuMat<uchar>(mask), colour_thresh);

}


