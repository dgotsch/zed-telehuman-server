#version 450
#define mediump

#extension GL_OES_standard_derivatives : enable

in vec2 f_texcoord;
in vec3 v_coord;
in float do_draw;
uniform sampler2D coltexture;

vec3 normals(vec3 pos) {
  vec3 fdx = dFdx(pos);
  vec3 fdy = dFdy(pos);
  return normalize(cross(fdx, fdy));
}

void main(void) {
	//vec2 f2_texcoord = f_texcoord*2.0 ;//vec2(0.0001, f_texcoord.y);
	vec3 nor = normals(v_coord);
	gl_FragColor = texture2D(coltexture, f_texcoord);
	if( do_draw < 0.999999 ) discard;
	//gl_FragColor.rgb = nor;
}
