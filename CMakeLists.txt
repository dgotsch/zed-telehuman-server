SET(execName ZED_Telehuman_Server)

CMAKE_MINIMUM_REQUIRED(VERSION 2.4)

if(COMMAND cmake_policy)
	cmake_policy(SET CMP0003 OLD)
	cmake_policy(SET CMP0015 OLD)
endif(COMMAND cmake_policy)

SET(EXECUTABLE_OUTPUT_PATH ".")

SET(VERSION_REQ_OCV "3.2")
SET(VERSION_REQ_CUDA "8.0")

IF(WIN32) # Windows
    message(FATAL_ERROR "Multi ZED not available on Windows")
    EXIT()
ELSE() # Linux

 	SET(SPECIAL_OS_LIBS "pthread" "X11")
    add_definitions(-Wno-write-strings)
ENDIF(WIN32)


find_package(ZED 2.0 REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(OpenCV ${VERSION_REQ_OCV} COMPONENTS core highgui imgproc video imgcodecs cudaimgproc cudaarithm cudawarping calib3d cudev REQUIRED)
find_package(CUDA ${VERSION_REQ_OCV} REQUIRED)
find_package(GLUT REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)

include_directories(${OpenGL_INCLUDE_DIRS})
include_directories(${GLEW_INCLUDE_DIRS})
include_directories(${FREEGLUT_INCLUDE_DIRS})    
include_directories(${CUDA_INCLUDE_DIRS})
include_directories(${ZED_INCLUDE_DIRS})
include_directories(${OpenCV_INCLUDE_DIRS})
include_directories(${EIGEN3_INCLUDE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
#include_directories(/usr/local/cuda-8.0/samples/common/inc)
#include_directories(ccalib/include)


link_directories(${OpenGL_LIBRARY_DIRS})
link_directories(${GLUT_LIBRARY_DIRS})
link_directories(${GLEW_LIBRARY_DIRS})

link_directories(${ZED_LIBRARY_DIR})
link_directories(${OpenCV_LIBRARY_DIRS})
link_directories(${CUDA_LIBRARY_DIRS})
link_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib)

SET(SRC_FOLDER src)
FILE(GLOB_RECURSE SRC_FILES "${SRC_FOLDER}/*.cpp")
FILE(GLOB_RECURSE SRC_CU_FOLDER "${SRC_FOLDER}/*.cu")


list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_50,code=sm_50)
list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_50,code=sm_52)
list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_50,code=sm_53) 
list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_60)
list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_61)


set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} -std=c++11)
cuda_add_executable(${execName} ${SRC_FILES} ${SRC_CU_FOLDER})


#ADD_EXECUTABLE(${execName} ${SRC_FILES})
#set_property(TARGET ${execName} PROPERTY OUTPUT_NAME "ZED Telehuman Server")
#add_definitions(-std=c++0x)

set_property(TARGET ${execName} PROPERTY OUTPUT_NAME "ZED Telehuman Server")
add_definitions(-std=c++11 -g -O3)
add_definitions(${OpenGL_DEFINITIONS})
add_definitions(${GLUT_DEFINITIONS})
add_definitions(${GLEW_DEFINITIONS})

TARGET_LINK_LIBRARIES(${execName}
                        ${ZED_LIBRARIES}
                        ${SPECIAL_OS_LIBS}
                        ${OpenCV_LIBRARIES}
                        ${CUDA_LIBRARIES} ${CUDA_npps_LIBRARY} ${CUDA_nppi_LIBRARY}
                        ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} ${GLEW_LIBRARY} 
                    )

#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O3 -lpthread" ) # Release Perf mode
