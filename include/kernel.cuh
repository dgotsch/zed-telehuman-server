// standard includes
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <sys/time.h>
//#include <algorithm>

// zed sdk
#include <sl/Camera.hpp>


//opencv includes
#include <opencv2/core/core.hpp>
#include <opencv2/cudaimgproc.hpp>
//#include <gpumat.hpp>



//opengl includes
#define GLM_FORCE_CUDA
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

// cuda includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <npp.h>
#include <device_functions.h>



using namespace glm;


// Normalize a float depth image and output it as a float or uchar4 image
void cuNormalizeDepth(sl::Mat &depth_meas, cv::cuda::GpuMat &depth_norm, float min_val, float max_val);
void cuNormalizeDepthToUchar4(sl::Mat &depth_meas,  cv::cuda::GpuMat &depth_norm, float min_val, float max_val);

// Mask off areas of the image that are considered to be outside a 3D region of interest
void cuMask3DSpace(sl::Mat &points, cv::cuda::GpuMat &mask, glm::mat4 to_world);
void cuMask3DSpace(sl::Mat &points, cv::cuda::GpuMat &mask, sl::CameraParameters params, mat4 cam_to_world );


void cuThresholdDifference(cv::cuda::GpuMat &image1, cv::cuda::GpuMat &image2, cv::cuda::GpuMat &mask, float colour_thresh);
