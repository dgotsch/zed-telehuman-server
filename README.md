# zed-telehuman-server

This code runs the server for depth camera capture of [Telehuman 2](https://www.youtube.com/watch?v=PNHWPB0he2k)

It is based on the ZED depth cameras and sends 3D calibrated colour and depth frames over UDP (to reduce latency) to a set of 45 Odroid SoCs, each rendering a unique perspective of the lightfield display.

## Features
The code includes the following:

-  3D pose estimation (of a calibration board) used to register the depth cameras relative to each other
-  (CUDA) GPU accelerated background subtraction based on both colour and depth data
-  (CUDA) GPU accelerated depth conversion into useful range that can be sent efficiently
-  multi-threaded, multi-GPU, 30 FPS, low latency capture and broadcast of 720p color and 540p depth data (limited on rendering side)
-  (unused) OpenGL rendere which renders the colour+depth frames as triangle meshes



## Warnings

- **This only works on Linux** (due to ZED SDKs linux-only support for multiple cameras)
- A high-end NVIDIA GPU card is needed with enough memory (around 2GB) for multiple ZED camera depth reconsttruction.
- If the frames appeared corrupted (green/purple, tearing) it means that the USB 3.0 controller is not fast enough to handle multiples ZEDs or the framerate chosen is too high.
- This code was written on Ubuntu 16.04.


## Requirements

- CMAKE 2.4+
- CUDA 8.0+
- OpenCV 3.2+ with OpenGL interop and CUDA modules
- ZED SDK 2.0
- [GLM](https://glm.g-truc.net/0.9.9/index.html)


## Building

Open a terminal and execute the following commands:
```bash
mkdir build
cd build
cmake ..
make
```

## Run the program
```bash
./ZED\ Telehuman\ Server
```
